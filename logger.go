package logger

import (
	"io"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"

	"gopkg.in/natefinch/lumberjack.v2"
)

const (

)

func Get() zerolog.Logger {
	var log zerolog.Logger
	var once sync.Once

    err := godotenv.Load(".env")
    if err != nil {    
        log.Fatal().Msg("Error loading .env file")
    }

    once.Do(func() {
        zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
        zerolog.TimeFieldFormat = time.RFC3339Nano

        logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))
        if err != nil {
            logLevel = int(zerolog.InfoLevel)
        }

        var output io.Writer = zerolog.ConsoleWriter{
            Out:        os.Stdout,
            TimeFormat: time.RFC3339,
        }

        if os.Getenv("APP_ENV") != "development" {
            fileLogger := &lumberjack.Logger{
                Filename:   getStrEnv("LOG_DIR", "/logs/my_logs_api"),
                MaxSize:    getIntEnv("LOG_MAX_SIZE", 5),
                MaxBackups: getIntEnv("LOG_MAX_BACKUPS", 10),
                MaxAge:     getIntEnv("LOG_MAX_AGE", 14),
                Compress:   getBoolEnv("LOG_COMPRESS", false),
            }

            output = zerolog.MultiLevelWriter(os.Stderr, fileLogger)
        }

        var gitRevision string

        log = zerolog.New(output).
            Level(zerolog.Level(logLevel)).
            With().
            Timestamp().
            Str("git_revision", gitRevision).
            Logger()
    })

    return log
}

func getStrEnv(key, def string) string {
    v := os.Getenv(key)
    if v == "" {
        return def
    }

    return v
}

func getIntEnv(key string, def int) int {
    v := os.Getenv(key)
    if v == "" {
        return def
    }

    i, err := strconv.Atoi(v)
    if err != nil {
        return def
    }

    return i
}

func getBoolEnv(key string, def bool) bool {
    switch os.Getenv(key) {
    case "true": return true    
    default: return false
    }
}
