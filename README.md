# Configuration
`LOG_DIR` with default value: `/logs/games_management_api.log`
`LOG_MAX_SIZE` is the maximum size in megabytes of the log file before it gets rotated. Default `5`.
`LOG_MAX_BACKUPS` is the maximum number of old log files to retain. Default `10`.
`LOG_MAX_AGE` is the maximum number of days to retain old log files based on the timestamp encoded in their filename. Default `14`.
`LOG_COMPRESS` determines if the rotated log files should be compressed using gzip. Default `false`.
